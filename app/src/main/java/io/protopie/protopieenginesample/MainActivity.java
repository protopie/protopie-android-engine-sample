package io.protopie.protopieenginesample;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executors;

import io.protopie.engine.model.enums.PPMsgChannel;
import io.protopie.engine.model.event.PPMsg;
import io.protopie.engine.pie.PPFontManager;
import io.protopie.engine.pie.PPPie;
import io.protopie.engine.pie.PPPieDescriptor;
import io.protopie.engine.pie.PPPieManager;
import io.protopie.engine.play.PPMsgSender;
import io.protopie.engine.play.PPPieView;

/**
 * A sample Activity for demonstrating how to use ProtoPie Engine SDK
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String TAG_STATE_FRAGMENT = "MainActivityStateFragment";
    private static final String PROTOPIE_LICENSE_KEY = "KyuSGY6KttPT4h2U8rrvMI4FR5IdBcFabVDJFppcQhYpbaVXqNy+jwuPw2JQcnuvxNQWGVNeEi9gnkPvkj/WlUjQfXWokAhx2lsSeOWifihaliqfDZIJdxarT3/XVeWs55MKvM0G20wi5n3Q5yziNBMpHWtV1QXxoIaUZwAQtnZyPGu/3+MS2xpYxQ==";
    private static final String SAMPLE_PIE_NAME = "sample";
    private static final String SAMPLE_FONT_NAME = "OpenSans-BoldItalic";

    private PPPieView pieView;

    private ProgressBar progressBar;

    private States states;
    private PPPieManager pieManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
        initMessageReceiver();
        initPieManager();
        initStateFragment();
        startFromState();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pieView.setMsgSender(null);
        pieView.stop();

        if (states.getLoadPieTask() != null) {
            states.getLoadPieTask().attachedActivity = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // When send_message item selected in the Activity Menu, send a message to Pie
        if (item.getItemId() == R.id.send_message) {
            if (states.getPie() != null) {
                pieView.sendMessageToPie("startAnimation", PPMsgChannel.STUDIO);
            }
            return true;
        } else if (item.getItemId() == R.id.reinstall_pie) {
            PPPieDescriptor pd = pieManager.getPieDescriptorByName(SAMPLE_PIE_NAME);
            if (pd != null) {
                pieManager.remove(pd);
                startLoadTask();
            }
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void initViews() {
        setContentView(R.layout.activity_main);
        pieView = findViewById(R.id.pieView);
        progressBar = findViewById(R.id.progressBar);
    }

    private void initMessageReceiver() {
        // Set up receiving messages from the Pie
        pieView.setMsgSender(new PPMsgSender() {
            @Override
            public void send(PPMsgChannel ppMsgChannel, PPMsg ppMsg) {
                Toast.makeText(MainActivity.this, "Message received from the Pie: " + ppMsg.getMessageId(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initPieManager() {
        pieManager = PPPieManager.getInstance(this);
        pieManager.applyLicense(PROTOPIE_LICENSE_KEY);
        pieManager.setIoTaskExecutor(Executors.newSingleThreadExecutor());

        PPFontManager fontManager = pieManager.getFontManager();
        if (!fontManager.hasFont(SAMPLE_FONT_NAME)) {
            InputStream inputStream = getResources().openRawResource(R.raw.opensans_bolditalic);
            try {
                fontManager.installFont(inputStream);
            } catch (Exception e) {
                Log.e(TAG, "Failed to install font", e);
                Toast.makeText(this, "Failed to install font", Toast.LENGTH_LONG).show();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
        }
    }

    private void startFromState() {
        if (states.getPie() == null) {
            if (states.getLoadPieTask() == null) {
                Log.i(TAG, "Pie is not loaded and a loading task is not started.");
                startLoadTask();
                showProgress();
            } else {
                Log.i(TAG, "Pie is not loaded, but a loading task is started.");
                states.getLoadPieTask().attachedActivity = this;
                showProgress();
            }
        } else {
            Log.i(TAG, "Pie is loaded.");
            showPie(states.getPie());
        }
    }

    private void initStateFragment() {
        FragmentManager fm = getSupportFragmentManager();
        states = (States) fm.findFragmentByTag(TAG_STATE_FRAGMENT);
        if (states == null) {
            states = new States();
            fm.beginTransaction().add(states, TAG_STATE_FRAGMENT).commit();
        }
    }

    private void startLoadTask() {
        LoadPieTask task = new LoadPieTask(pieManager, getResources());
        task.attachedActivity = this;
        task.execute();

        states.setLoadPieTask(task);
    }

    private void notifyPieLoadingResult(PPPie pie) {
        hideProgress();

        if (pie != null) {
            states.setPie(pie);
            showPie(pie);
        } else {
            showErrorMessage("Failed to load the sample pie.");
        }
    }

    private void showPie(PPPie pie) {
        pieView.setPie(pie);
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    /**
     * AsyncTask to load the sample pie
     */
    private static class LoadPieTask extends AsyncTask<Void, Void, PPPie> {
        private final PPPieManager pieManager;
        private final Resources resources;
        private MainActivity attachedActivity;

        LoadPieTask(PPPieManager pieManager, Resources resources) {
            this.pieManager = pieManager;
            this.resources = resources;
        }

        @Override
        protected PPPie doInBackground(Void... params) {
            Log.i(TAG, "Loading sample pie");
            try {
                return loadSamplePie();
            } catch (IOException e) {
                Log.e(TAG, "Failed to add or load the sample pie.", e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(PPPie pie) {
            if (attachedActivity != null) {
                attachedActivity.notifyPieLoadingResult(pie);
            }
        }

        private PPPie loadSamplePie() throws IOException {
            PPPieDescriptor pd = pieManager.getPieDescriptorByName(SAMPLE_PIE_NAME);
            if (pd == null) {
                Log.i(TAG, "Adding a sample pie to PieManager");
                InputStream inputStream = resources.openRawResource(R.raw.sample);
                try {
                    pd = pieManager.addFromFile(inputStream, SAMPLE_PIE_NAME);
                } finally {
                    IOUtils.closeQuietly(inputStream);
                }
            } else {
                Log.i(TAG, "Reusing the previously added pie");
            }
            return pieManager.loadLocalPie(pd);
        }
    }

    /**
     * A Fragment for holding states of MainActivity to retain them on recreation of Activity
     */
    public static class States extends Fragment {
        /**
         * Async task to load the sample pie
         */
        private LoadPieTask loadPieTask;

        /**
         * Loaded Pie
         */
        private PPPie pie;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public LoadPieTask getLoadPieTask() {
            return loadPieTask;
        }

        public void setLoadPieTask(LoadPieTask loadPieTask) {
            this.loadPieTask = loadPieTask;
        }

        public PPPie getPie() {
            return pie;
        }

        public void setPie(PPPie pie) {
            this.pie = pie;
        }
    }
}
